import re

from flask import g
from flask import request
from flask import abort


def error_parameter(message):
    return message, 400


def error_logic(message):
    return message, 403


def _request_get(pname, ptype, uselist=False):
    if ptype == 'str' or ptype == 'text':
        val_list = request.values.getlist(pname, type=str)
        val_list = map(lambda val: val.strip(), val_list)
    elif ptype == 'int':
        val_list = request.values.getlist(pname, type=int)
    elif ptype == 'float':
        val_list = request.values.getlist(pname, type=float)
    elif ptype == 'bool':
        val_list = request.values.getlist(pname, type=str)
        bool_mapping = {'true': True, 'false': False}
        val_list = map(lambda val: bool_mapping.get(val.lower()), val_list)
    elif ptype == 'file':
        val_list = request.files.getlist(pname)
    else:
        val_list = []

    val_list = filter(lambda val: val is not None, val_list)
    val_list = list(val_list)

    if not val_list:
        return None

    if not uselist:
        return val_list[0]

    return val_list


def request_get_all(name_validators_dict):

    class RequestParams():
        pass

    q = RequestParams()

    invalid_params = dict()

    def resolve_name(name):
        pname, ptype = name.split(':')

        prequired = False
        if pname.startswith('*'):
            pname = pname[1:]
            prequired = True

        puselist = False
        if pname.endswith('[]'):
            pname = pname[:-2]
            puselist = True

        return (pname, ptype, prequired, puselist)

    # Step 1 : Get value of each parameter
    params = []
    for name, _ in name_validators_dict.items():
        pname, ptype, prequired, puselist = resolve_name(name)
        pval = _request_get(pname, ptype, puselist)

        setattr(q, pname, pval)
        params.append((name, pname, pval, prequired))

    # Step 2 : Validate value of each parameter
    for name, pname, pval, prequired in params:
        if prequired and pval is None:
            invalid_params[pname] = 'required'
            continue

        for validator, msg in name_validators_dict[name]:
            if not validator(pval):
                invalid_params[pname] = msg() if callable(msg) else msg
                break

    if invalid_params:
        g.message = str(invalid_params)
        abort(400)

    return q


def check_length(minlength=None, maxlength=None):
    def validator(val):
        if minlength is not None and len(val) < minlength:
            return False
        if maxlength is not None and maxlength < len(val):
            return False
        return True

    if minlength is not None and maxlength is not None:
        return (validator, 'should be between {} and {} letters'.format(minlength, maxlength))
    elif minlength is not None:
        return (validator, 'should be at least {} letters'.format(minlength))
    elif maxlength is not None:
        return (validator, 'should be at most {} letters'.format(maxlength))
    else:
        return (validator, '')  # never be False


def check_url():
    def validator(val):
        return re.match(
            r'^(?:http|ftp)s?://'  # http:// or https://
            r'(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\.)+'  # domain
            r'(?:[a-zA-Z]{2,6}\.?|[a-zA-Z0-9-]{2,}\.?)|'  # domain
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$',
            val)

    return (validator, 'is invalid url format')
