import re

from sqlalchemy import types as dbtypes
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import relationship as Relationship
from sqlalchemy.schema import Column, ForeignKey as _ForeignKey, Index

from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def _build_table_name(class_name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', class_name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def ForeignKey(class_name, key='id'):
    return _ForeignKey(_build_table_name(class_name) + '.' + key)


class ModelBase(db.Model):
    __abstract__ = True

    @declared_attr
    def __tablename__(cls):
        return _build_table_name(cls.__name__)


class UserModel(ModelBase):
    PW_HASH_CONF = 'pbkdf2:sha256:2000'

    id = Column(dbtypes.Integer, nullable=False, primary_key=True)

    userid = Column(dbtypes.String(32), nullable=False, unique=True)
    userpw = Column(dbtypes.String(128), nullable=False)


class WebHookModel(ModelBase):
    id = Column(dbtypes.Integer, nullable=False, primary_key=True)

    user_id = Column(ForeignKey('UserModel'),
                     nullable=False)
    user = Relationship('UserModel', uselist=False)

    uid = Column(dbtypes.String(32), nullable=False)

    converter_name = Column(dbtypes.String(64), nullable=False)

    slack_username = Column(dbtypes.String(32), nullable=True)
    slack_webhook = Column(dbtypes.String(512), nullable=False)

Index('ix_user_id_uid',
      WebHookModel.user_id,
      WebHookModel.uid,
      unique=True)
