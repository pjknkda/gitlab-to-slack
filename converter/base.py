import re


class BaseHook(object):
    EMPTY_REF = '0000000000000000000000000000000000000000'

    HTML_PATTERN = r'''<a (?:.*?)href=['"](.+?)['"](?:.*?)>(.+?)<\/a>'''
    MARKDOWN_PATTERN = r'''\[(.*?)\]\((.+?)\)'''

    @classmethod
    def is_tag_ref(cls, ref):
        return ref.startswith('refs/tags/')

    @classmethod
    def is_blank_ref(cls, ref):
        return (ref == cls.EMPTY_REF)

    @classmethod
    def truncate_sha(cls, sha):
        return sha[:8]

    @classmethod
    def truncate_tag_ref(cls, ref):
        return '/'.join(ref.split('/')[2:])

    @classmethod
    def title_format(cls, text):
        return text.splitlines()[0].strip()

    @classmethod
    def link_format(cls, text):

        if text is None:
            return None

        def _html_sub(matchobj):
            return "<{link}|{text}>".format(
                link=matchobj.group(1),
                text=matchobj.group(2)
            )

        def _markdown_sub(matchobj):
            return "<{link}|{text}>".format(
                link=matchobj.group(2),
                text=matchobj.group(1)
            )

        text = re.sub(cls.HTML_PATTERN, _html_sub, text)
        text = re.sub(cls.MARKDOWN_PATTERN, _markdown_sub, text)

        return text

    def __init__(self, msg):
        raise NotImplementedError

    @property
    def message(self):
        return BaseHook.link_format(self._pretext), self._attachments

    @property
    def _pretext(self):
        raise NotImplementedError

    @property
    def _attachments(self):
        raise NotImplementedError
