from .base import BaseHook


class PushHook(BaseHook):

    def __init__(self, msg):
        params = dict(
            after=msg.get('after', BaseHook.EMPTY_REF),
            before=msg.get('before', BaseHook.EMPTY_REF),
            trun_after=BaseHook.truncate_sha(msg.get('after', BaseHook.EMPTY_REF)),
            trun_before=BaseHook.truncate_sha(msg.get('before', BaseHook.EMPTY_REF)),
            commits=msg.get('commits', []),
            project_name=msg.get('repository', {}).get('name', ''),
            project_url=msg.get('repository', {}).get('homepage', ''),
            ref=BaseHook.truncate_tag_ref(msg.get('ref', '')),
            ref_type='tag' if BaseHook.is_tag_ref(msg.get('ref', '')) else 'branch',
            user_name=msg.get('user_name')
        )

        params['branch_url'] = "{project_url}/commits/{ref}".format(**params)
        params['compare_url'] = "{project_url}/compare/{trun_before}...{trun_after}".format(**params)

        params['branch_link'] = "[{ref}]({branch_url})".format(**params)
        params['project_link'] = "[{project_name}]({project_url})".format(**params)
        params['compare_link'] = "[Compare changes]({compare_url})".format(**params)

        self.params = params

    @property
    def _pretext(self):
        if BaseHook.is_blank_ref(self.params['before']):
            # new brnach
            return "{user_name} pushed new {ref_type} {branch_link} to {project_link}".format(**self.params)
        elif BaseHook.is_blank_ref(self.params['after']):
            # removed branch
            return "{user_name} removed {ref_type} {ref} from {project_link}".format(**self.params)
        else:
            # push message
            return "{user_name} pushed to {ref_type} {branch_link} of {project_link} ({compare_link})".format(**self.params)

    @property
    def _attachments(self):
        if BaseHook.is_blank_ref(self.params['before']):
            # new brnach
            return []
        elif BaseHook.is_blank_ref(self.params['after']):
            # removed branch
            return []
        else:
            # push message
            commit_messages = []
            for commit in self.params['commits']:
                commit_params = dict(
                    id=BaseHook.truncate_sha(commit.get('id', '')),
                    url=commit.get('url', ''),
                    message=commit.get('message', ''),
                    author=commit.get('author', {}).get('name', '')
                )

                commit_messages.append("[{id}]({url}): {message} - {author}".format(**commit_params))

            return [{
                'text': BaseHook.link_format('\n'.join(commit_messages)),
                'color': '#345'
            }]


class IsuueHook(BaseHook):

    def __init__(self, msg):
        params = dict(
            user_name=msg.get('user', {}).get('name', ''),
            project_name=msg.get('repository', {}).get('name', ''),
            project_url=msg.get('repository', {}).get('homepage', '')
        )

        obj_attr = msg.get('object_attributes', {})

        params.update({
            'title': obj_attr.get('title', ''),
            'issue_iid': obj_attr.get('iid', 0),
            'issue_url': obj_attr.get('url', ''),
            'action': obj_attr.get('action', ''),
            'state': obj_attr.get('state', ''),
            'description': obj_attr.get('description', ''),
        })

        params['project_link'] = "[{project_name}]({project_url})".format(**params)
        params['issue_link'] = "[issue #{issue_iid}]({issue_url})".format(**params)

        self.params = params

    @property
    def is_update(self):
        return (self.params['action'] == 'update')

    @property
    def _pretext(self):
        if self.is_update:
            return None

        return "{user_name} {state} {issue_link} in {project_link}: *{title}*".format(**self.params)

    @property
    def _attachments(self):
        if self.is_update:
            return None

        if self.params['action'] != 'open':
            return []
        else:
            return [{
                'text': BaseHook.link_format("{description}".format(**self.params)),
                'color': '#345'
            }]


class NoteHook(BaseHook):

    def __init__(self, msg):
        params = dict(
            user_name=msg.get('user', {}).get('name', ''),
            project_name=msg.get('repository', {}).get('name', ''),
            project_url=msg.get('repository', {}).get('homepage', '')
        )

        obj_attr = msg.get('object_attributes', {})

        params.update({
            'note': obj_attr.get('note', ''),
            'note_url': obj_attr.get('url', '')
        })

        params['project_link'] = "[{project_name}]({project_url})".format(**params)

        self.noteable_type = obj_attr.get('noteable_type', '')

        if self.noteable_type == 'Commit':
            commit = msg.get('commit', {})

            params.update({
                'commit_sha': BaseHook.truncate_sha(commit.get('id', '')),
                'title': BaseHook.title_format(commit.get('message', ''))
            })

            params['commit_link'] = "[commit {commit_sha}]({note_url})".format(**params)

        elif self.noteable_type == 'Issue':
            issue = msg.get('issue', {})

            params.update({
                'issue_iid': issue.get('iid', 0),
                'title': BaseHook.title_format(issue.get('title', ''))
            })

            params['note_link'] = "[issue #{issue_iid}]({note_url})".format(**params)

        elif self.noteable_type == 'MergeRequest':
            merge_request = msg.get('merge_request', {})

            params.update({
                'merge_request_id': merge_request.get('iid', 0),
                'title': BaseHook.title_format(merge_request.get('title', ''))
            })

            params['merge_request_link'] = "[merge request #{merge_request_id}]({note_url})".format(**params)

        elif self.noteable_type == 'Snippet':
            snippet = msg.get('snippet', {})

            params.update({
                'snippet_id': snippet.get('iid', 0),
                'title': BaseHook.title_format(snippet.get('title', ''))
            })

            params['snippet_link'] = "[snippet #{snippet_id}]({note_url})".format(**params)

        self.params = params

    @property
    def _pretext(self):
        if self.noteable_type == 'Commit':
            return "{user_name} commented on {commit_link} in {project_link}: *{title}*".format(**self.params)
        elif self.noteable_type == 'Issue':
            return "{user_name} commented on {note_link} in {project_link}: *{title}*".format(**self.params)
        elif self.noteable_type == 'MergeRequest':
            return "{user_name} commented on {merge_request_link} in {project_link}: *{title}*".format(**self.params)
        elif self.noteable_type == 'Snippet':
            return "{user_name} commented on {snippet_link} in {project_link}: *{title}*".format(**self.params)

        return ""

    @property
    def _attachments(self):
        return [{
            'text': BaseHook.link_format("{note}".format(**self.params)),
            'color': '#345'
        }]


class MergeHook(BaseHook):

    def __init__(self, msg):
        params = dict(
            user_name=msg.get('user', {}).get('name', ''),
            project_name=msg.get('repository', {}).get('name', ''),
            project_url=msg.get('repository', {}).get('homepage', '')
        )

        obj_attr = msg.get('object_attributes', {})

        params.update({
            'action': obj_attr.get('action', ''),
            'merge_request_id': obj_attr.get('iid', 0),
            'state': obj_attr.get('state', ''),
            'title': BaseHook.title_format(obj_attr.get('title', ''))
        })

        params['merge_request_url'] = "{project_url}/merge_requests/{merge_request_id}".format(**params)

        params['project_link'] = "[{project_name}]({project_url})".format(**params)
        params['merge_request_link'] = "[merge request #{merge_request_id}]({merge_request_url})".format(**params)

        self.params = params

    @property
    def is_update(self):
        return (self.params['action'] == 'update')

    @property
    def _pretext(self):
        if self.is_update:
            return None

        return "{user_name} {state} {merge_request_link} in {project_link}: {title}".format(**self.params)

    @property
    def _attachments(self):
        if self.is_update:
            return None

        return []

MESSAGE_TEMPLATE = {
    # Push events (Push Hook)
    'PUSH': PushHook,

    # Tag events (Tag Push Hook)
    'TAG': PushHook,

    # Issues events (Issue Hook)
    'ISSUE': IsuueHook,

    # Note events (Note Hook)
    'NOTE': NoteHook,

    # Merge request events (Merge Request Hook)
    'MERGE_REQUEST': MergeHook
}
