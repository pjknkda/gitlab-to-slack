from config import *
from request_func import *

import logging
logger = logging.getLogger(LOGGING_LOGGER_NAME)

import datetime
import glob
import importlib
import os
import random
import requests
import string
import json
import hashlib
import collections

from functools import wraps

from flask import Flask
from flask import session, g
from flask import request, url_for
from flask import render_template, redirect, abort
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI
app.config['SECRET_KEY'] = SESSION_SECRET_KEY

import model as m
db = m.db
db.init_app(app)


def _hash_userpw(userpw):
    return generate_password_hash(userpw,
                                  m.UserModel.PW_HASH_CONF)


def _random_string(length):
    letters = string.ascii_letters + string.digits
    return ''.join((random.choice(letters) for _ in range(length)))

with app.app_context():
    db.create_all()

    # if m.UserModel.query.count() == 0:
    #     logger.info('Make a default admin user.')

    #     db.session.add(m.UserModel(
    #         userid='admin',
    #         userpw=_hash_userpw('supersecurepassword')
    #     ))
    #     db.session.commit()

converter_files = glob.glob('converter/*.py')
converter_names = set([os.path.basename(f)[:-3] for f in converter_files]) - set(['base'])
converters = dict()
for converter_name in converter_names:
    try:
        converter_obj = importlib.import_module('converter.' + converter_name, __name__)
    except:
        logger.exception('Cannot load converter {}'.format(converter_name))
        continue

    if not hasattr(converter_obj, 'MESSAGE_TEMPLATE'):
        logger.error('[{}] no MESSAGE_TEMPLATE'.format(converter_name))
        continue

    message_template = converter_obj.MESSAGE_TEMPLATE

    missing_message_template = set(['PUSH', 'TAG', 'ISSUE', 'NOTE', 'MERGE_REQUEST']) \
        - set(message_template.keys())
    if len(missing_message_template) != 0:
        logger.error('[{}] incomplete MESSAGE_TEMPLATE : missing {}'.format(
            converter_name,
            ', '.join(missing_message_template)
        ))
        continue

    converters[converter_name] = message_template


logger.info('Prepared converters: {}'.format(converter_names))

hash_queue = collections.deque(maxlen=100)

# Check template consistency
with app.app_context():
    for webhook in m.WebHookModel.query:
        if webhook.converter_name not in converter_names:
            logger.error('[WebHook {}] Template {} has not been found.'.format(webhook.id,
                                                                               webhook.converter_name))


def get_current_user():
    user_info = session.get('user')

    if user_info is None:
        return None

    if user_info.get('expire', datetime.datetime(1970, 1, 1)) < datetime.datetime.now():
        session['user'] = None
        return None

    try:
        return m.UserModel.query.get(user_info.get('user_id'))
    except:
        return None


def auth_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        user = get_current_user()

        if user is None:
            if request.is_xhr:
                return 'Authorization required', 401
            else:
                return redirect(url_for('index_routing'))

        return f(*args, **kwargs)
    return wrapped


@app.errorhandler(400)
def error_page_400(e):
    return getattr(g, 'message', ''), 400


@app.errorhandler(500)
def error_page_500(e):
    return getattr(g, 'message', ''), 500


@app.route('/', methods=['GET'])
def index_routing():
    return render_template('index.html')


@app.route('/login/', methods=['POST'])
def login_routing():
    q = request_get_all({
        '*userid:str': [
            check_length(4, 32)
        ],
        '*userpw:str': [
            check_length(8)
        ]
    })

    user = m.UserModel.query \
        .filter(m.UserModel.userid == q.userid) \
        .first()

    if user is None:
        return error_logic('No such user')

    if not check_password_hash(user.userpw, q.userpw):
        return error_logic('Wrong password')

    session['user'] = {
        'expire': datetime.datetime.now() + datetime.timedelta(seconds=SESSION_LIFETIME),
        'user_id': user.id
    }

    return redirect(url_for('main_routing'))


@app.route('/signup/', methods=['GET', 'POST'])
def signup_routing():
    if request.method == 'GET':
        return render_template('signup.html')

    q = request_get_all({
        '*userid:str': [
            check_length(4, 32)
        ],
        '*userpw:str': [
            check_length(8)
        ],
        '*join_key:str': []
    })

    if q.join_key != USER_JOIN_KEY:
        return error_logic('Wrong join key')

    dup_user = m.UserModel.query \
        .filter(m.UserModel.userid == q.userid) \
        .first()

    if dup_user is not None:
        return error_logic('Same userid is already in user')

    user = m.UserModel(
        userid=q.userid,
        userpw=generate_password_hash(q.userpw,
                                      m.UserModel.PW_HASH_CONF)
    )

    db.session.add(user)

    try:
        db.session.commit()
    except:
        logger.exception('DB error')
        db.session.rollback()
        abort(500)

    return redirect(url_for('index_routing'))


@app.route('/logout/', methods=['GET', 'POST'])
@auth_required
def logout_routing():
    session['user'] = None
    del session['user']

    return redirect(url_for('index_routing'))


@app.route('/main/', methods=['GET'])
@auth_required
def main_routing():
    me_user = get_current_user()

    webhooks = m.WebHookModel.query \
        .filter(m.WebHookModel.user_id == me_user.id) \
        .order_by(m.WebHookModel.id.asc()) \
        .all()

    return render_template('main.html', webhooks=webhooks,
                           converter_names=converter_names)


@app.route('/main/webhook/add/', methods=['POST'])
@auth_required
def main_webhook_add_routing():
    me_user = get_current_user()

    q = request_get_all({
        '*converter_name:str': [
            check_length(1, 64)
        ],
        '*slack_username:str': [
            check_length(1)
        ],
        '*slack_webhook:str': [
            check_url()
        ]
    })

    if converter_name not in converter_names:
        return error_logic('No such converter')

    webhook = m.WebHookModel(
        user=me_user,
        uid=_random_string(32),
        converter_name=q.converter_name,
        slack_username=q.slack_username,
        slack_webhook=q.slack_webhook
    )

    db.session.add(webhook)

    try:
        db.session.commit()
    except:
        logger.exception('DB error')
        db.session.rollback()
        abort(500)

    return redirect(url_for('main_routing'))


@app.route('/main/webhook/delete/', methods=['POST'])
@auth_required
def main_webhook_delete_routing():
    me_user = get_current_user()

    q = request_get_all({
        '*uid:str': [
            check_length(32, 32)
        ]
    })

    webhook = m.WebHookModel.query \
        .filter(m.WebHookModel.uid == q.uid) \
        .first()

    if webhook is None:
        return error_logic('Invalid uid')

    if webhook.user != me_user:
        return error_logic('Not yours')

    db.session.delete(webhook)

    try:
        db.session.commit()
    except:
        logger.exception('DB error')
        db.session.rollback()
        abort(500)

    return redirect(url_for('main_routing'))


@app.route('/webhook/<userid>/<webhook_uid>', methods=['GET', 'POST'])
def webhook_routing(userid, webhook_uid):
    webhook = m.WebHookModel.query \
        .join(m.UserModel,
              m.UserModel.id == m.WebHookModel.user_id) \
        .filter(
            (m.UserModel.userid == userid) &
            (m.WebHookModel.uid == webhook_uid)
        ) \
        .first()

    if webhook is None:
        abort(404)

    if request.method == 'GET':
        return 'Valid URL :)'

    gitlab_event_header = request.headers.get('X-Gitlab-Event', '')

    try:
        request_body = json.loads(request.data.decode('utf-8'))
    except:
        return error_parameter('Invalid request body')

    converter = converters.get(webhook.converter_name)
    if converter is None:
        return error_logic('Invalid converter')

    slackform = None
    if gitlab_event_header == 'Push Hook':
        slackform = converter['PUSH'](request_body)
    elif gitlab_event_header == 'Tag Push Hook':
        slackform = converter['TAG'](request_body)
    elif gitlab_event_header == 'Issue Hook':
        slackform = converter['ISSUE'](request_body)
    elif gitlab_event_header == 'Note Hook':
        slackform = converter['NOTE'](request_body)
    elif gitlab_event_header == 'Merge Request Hook':
        slackform = converter['MERGE_REQUEST'](request_body)
    else:
        return error_parameter('Invalid request header : X-Gitlab-Event')

    if slackform is None:
        return error_logic('Failed to regenerate webhook message')

    slack_msg, slack_attachments = slackform.message

    slack_req = {
        'username': webhook.slack_username,
        'text': slack_msg,
        'attachments': slack_attachments
    }

    data = json.dumps(slack_req, sort_keys=True)
    data_hash = hashlib.sha512(data.encode('utf-8')).digest()

    if data_hash not in hash_queue:
        hash_queue.append(data_hash)

        r = requests.post(
            webhook.slack_webhook,
            data=data
        )

        if r.status_code != requests.codes.ok:
            return error_logic('Failed to send to slack webhook')

    return 'Success'
