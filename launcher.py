import logging
import sys

from config import *

# setting logger
logger = logging.getLogger(LOGGING_LOGGER_NAME)
logger.addHandler(logging.StreamHandler(sys.stdout))

if LOGGING_FILE_LOCATION is not None:
    try:
        from logging.handlers import RotatingFileHandler
        log_handler = RotatingFileHandler(LOGGING_FILE_LOCATION,
                                          maxBytes=LOGGING_FILE_MAX_BYTES,
                                          backupCount=10)
        log_handler.setFormatter(logging.Formatter(LOGGING_FORMAT))
        logger.addHandler(log_handler)
    except:
        logger.exception('Cannot make log file.')

if LOGGING_SLACK_WEBHOOK_URL is not None:
    from logging.handlers import HTTPHandler

    class WebhookHTTPHandler(HTTPHandler):

        def mapLogRecord(self, record):
            import json

            formatter = logging.Formatter(LOGGING_FORMAT)
            formatted_log = formatter.format(record)

            return {
                'payload': json.dumps({
                    'text': formatted_log
                })
            }

    from urllib.parse import urlparse
    webhook_url_parsed = urlparse(LOGGING_SLACK_WEBHOOK_URL)

    log_handler = WebhookHTTPHandler(
        host=webhook_url_parsed[1],
        url=webhook_url_parsed[2],
        method='POST',
        secure=webhook_url_parsed[0] == 'https'
    )
    log_handler.setLevel(logging.ERROR)
    logger.addHandler(log_handler)

logger.setLevel(logging.INFO)
logger.info('Logger is started.')

from worker import app


if __name__ == '__main__':
    try:
        app.run('0.0.0.0', port=15622, debug=True)
    except KeyboardInterrupt:
        logger.info('Exited by keyboard interrupt.')
    except Exception as ex:
        logger.exception(ex)
